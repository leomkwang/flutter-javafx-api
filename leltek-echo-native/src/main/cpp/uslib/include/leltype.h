//
//  leltype.h
//  Runner
//
//  Created by Leow on 2021/3/24.
//

#ifndef leltype_h
#define leltype_h

struct GyroData {
    short int accel_x;
    short int accel_y;
    short int accel_z;
    short int gyro_x;
    short int gyro_y;
    short int gyro_z;
    unsigned long timer_global;    // [31:0]
};


#endif /* leltype_h */
