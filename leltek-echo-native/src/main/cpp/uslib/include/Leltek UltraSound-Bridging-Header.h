
#include "Listener.h"
#include "lelprop.h"
#include "leltype.h"

#if __ANDROID__

#define USCORE_EXPORT

#elif __APPLE__

#define USCORE_EXPORT

#elif __linux__

#ifdef USCORE_IMPL
#define USCORE_EXPORT __attribute__((visibility("default")))
#else
#define USCORE_EXPORT
#endif

#else

#ifdef USCORE_IMPL
#define USCORE_EXPORT __declspec(dllexport)
#else
#define USCORE_EXPORT __declspec(dllimport)
#endif

#define DLL_EXPORT __declspec(dllexport)

#endif

#if defined(__cplusplus)
extern "C" {
#endif

    USCORE_EXPORT int LelVersion();

    /**
     * Initialize core.
     * @param dir Working directory which stores initialization script.
     * @param _listener Callback function to get event from SDK.
     * @return True: success, False: fail
     */
    USCORE_EXPORT bool LelInitialize(const char* dir, Listener _listener);

    /**
     * Release all resources allocated by core.
     */
    USCORE_EXPORT void LelDestruct();

    /**
     * Connect probe.
     * @param type Device type
     * @param param Probe ID for dev_virtual.usb handle for dev_usb.
     * @return Result
     */
    USCORE_EXPORT bool LelConnect(DeviceType type, int param);

    /**
     * Disconnect probe.
     */
    USCORE_EXPORT void LelDisconnect();

    /**
     * Init probe registers.
     * @return Result
     */
    USCORE_EXPORT bool LelInitProbe();

    /**
     * Start to receive data.
     * @return
     */
    USCORE_EXPORT bool LelStartScan();

    /**
     * Stop receiving data.
     */
    USCORE_EXPORT void LelStopScan();

    /**
     * Get connection status.
     * @return
     */
    USCORE_EXPORT bool LelIsConnected();

    /**
     * Get scan status.
     * @return
     */
    USCORE_EXPORT bool LelIsStarted();

    /**
     * Set the value of the property specified by id.
     * @param id Property id.
     * @param data Value to set.
     * @return Result
     */
    USCORE_EXPORT bool LelSetProp(PropId id, void* data);

    /**
     * Get value of the property specified by id.
     * @param id Property id.
     * @param data Value to get.
     * @return Result
     */
    USCORE_EXPORT bool LelGetProp(PropId id, void* data);

    /**
     * Get values of the property specified by id.
     * @param id Property id.
     * @param data Value to get.
     * @return Result
     */
    USCORE_EXPORT int LelGetPropArray(PropId id, void* data);

    /**
     * Set the values of the property specified by id.
     * @param id Property id.
     * @param data Values to set.
     * @param len Array length.
     * @return
     */
    USCORE_EXPORT bool LelSetPropArray(PropId id, void* data, int len);

    /**
     * A generic lightweight JSON function with key-value pair format.
     *
     * @param method Method string
     *        getProbeInfo: Get probe infos
     *            Output: probeId, depth, RPx, origin, origin, theta, Ti, Mi, imgW, imgH, imgMW, imgMH, imgPWW, imgPWH, clibVer, fpgaRev
     *
     *        getDynamicInfo: Get changeable probe infos
     *            Output: dataRate, framerate, isBatteryCharging, batteryRemaining, boardTemperature
     *
     *        getErrInfo: get detailed error info if function fails
     *            Output: func: failed function; msg: failed message
     *
     *        getPassword: get password for given ssid
     *            Input: ssid: ssid
     *            Output: password: password
     *
     *        savePreset: save current preset params as new preset.
     *            Input: english: preset name; Icon: icon name
     *
     *        deletePreset: delete preset by id.
     *            Input: id: preset id.
     *
     *        updatePreset: update preset params with current params.
     *
     *        getPreset: Get preset infos.
     *            Input: index: preset index. Get total number of preset when assigned -1.
     *            Output:
     *                len: total number of preset when index assigned -1.
     *                id: preset ID.
     *                english: preset name.
     *                icon: preset icon string.
     *                user: specify user defined or not.
     *
     *        getParamTbl: Get allowed values of preset param.
     *            Input:
     *                name: param name.
     *                mode: scan mode (optional, current mode by default if not specified)
     *                    “0”: MODE_B, “1”: MODE_C,“2”: MODE_M, “3”: MODE_PW
     *            Output: tbl: allowed values.
     *
     *        GetParam: Get index or value of preset param.
     *            Input:
     *                name: param name.
     *                mode: scan mode (optional, current mode by default if not specified)
     *            Output: val: index or value.
     *
     *        SetParam: Set index or value of preset param.
     *            Input: name: param name. val: index or value to set.
     *            Return value: Result
     *            Supported Params:
     *                B mode: TXFreq, depth, gain, dynamicRange, enhanceLevel, grayMap, persistence, frameRate, beamformMethod
     *                M mode: gain, MPrf
     *                C mode: TXFreq, depth, colorPowerThreshold, colorBThreshold, colorAngle, colorWallFilter, colorPrf
     *                PW mode: gain, gatePWmode, PWAngle, PWPrf, PWBaseline
     *
     * @param input  Input text buffer.
     * @param output Output text buffer.
     * @return
     */
    USCORE_EXPORT bool LelInvokeMethod(const char* method, const char* input, char* output);

    USCORE_EXPORT int LelGetErrorCode();

    USCORE_EXPORT unsigned char *LelGetImageBuffer();   //b-mode
    USCORE_EXPORT unsigned char *LelGetImageMBuffer();
    USCORE_EXPORT unsigned char *LelGetImagePWBuffer();
    USCORE_EXPORT unsigned char *LelGetAudioBuffer();
    USCORE_EXPORT unsigned char *LelGetRawPWBuffer();
    USCORE_EXPORT unsigned char *LelGetRawBBuffer();
    USCORE_EXPORT unsigned char *LelGetRawCBuffer();
    USCORE_EXPORT bool LelGetGyroData(struct GyroData *data);

    USCORE_EXPORT void LelOnUsbConnection(bool connected);

    USCORE_EXPORT void LelSetPWRealTimeDrawLineDetails(unsigned int colorUp, unsigned int colorDown, short thickness, unsigned int colorCrest, unsigned int colorTrough);

    USCORE_EXPORT void LelSetMLeadingLineColor(unsigned int color);

    USCORE_EXPORT bool LelEnumUsb();

#if defined(__cplusplus)
}  // extern "C"
#endif
