//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//  (C) Copyright 2018 Leltek Inc.                                                      //
//  All Rights Reserved                                                                 //
//  Listener.h: Header file for listener data and its registration function             //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////
#ifndef __LISTENER_H__
#define __LISTENER_H__

#include <stdbool.h>

enum EventId {
    ON_NEW_IMAGE_READY, 	// B mode image ready. iParam: size in bytes
    ON_NEW_IMAGE_M_READY,   // M mode image ready. iParam: size in bytes
    ON_NEW_IMAGE_PW_READY,  // PW mode image ready. iParam: size in bytes
    ON_NEW_AUDIO_READY,     // todo: PW audio data ready. iParam: audio length? or sample rate?
    ON_NEW_RAW_PW_READY,    // PW raw data (IQ) ready. iParam: data length
    ON_NEW_RAW_B_READY,     // B mode raw data ready. iParam: data length (beamLine*512), beamLine is 128 or 256
    ON_NEW_RAW_C_READY,     // C mode raw data ready. iParam: data length (128*256)
    ON_GSENSOR_RECEIVE,		// G-sensor state notification. iParam: 0: none, 1: motion, 2: sleep
    ON_BATTERY_LEVEL_TOO_LOW_ERROR, // Battery level too low notification
    ON_TEMPERATURE_OVERHEAT_ERROR,  // Temperature overheat notification
    ON_CONNECTION,     // LelConnect() result. iParam: 0 = fail, 1 = successful
    ON_DISCONNECTION,  // Disconnection notification(TCP socket)
    ON_TCP_ERROR,
    ON_BUTTON,         // Freeze button status. iParam: 0 = released, 1 = pressed
    ON_INIT_PROGRESS,  // Report init progress in percentage. iParam: 0-100
    ON_INITED,         // LelInitProbe() result. iParam: 0 = fail, 1 = successful
    ON_USB_CONNECTION, // USB connect/disconnect event. iParam: 0 = connected, 1 = disconnected
    ON_GYRO_READY,     // GYRO data ready event
    ON_USB_ERROR,
};
typedef enum EventId EventId;

typedef void(*Listener)(EventId id, int iParam, float fParam);

#define TCP_TIMEOUT_CODE     -1

//major error code
enum UsStatus {
    us_unknown = -1,
    us_success = 0,
    us_miscError = 1,
    us_tcpError = 2,
    us_usbError = 3,
    us_sysError = 4,
};

//minor error code
enum MiscError {
    misc_invalidProbeId = 1,
    misc_ackPacketFormatError = 2,
    misc_tcpReconnectError = 3,
};

enum TcpError {
    tcp_timeout = -1,
    //0~    -> OS error code
};

enum UsbError {
    usb_permissionDenied = -1,
    //0~    -> OS error code
};

enum SysError {
    sys_batteryLevelTooLow = 1,
    sys_temperatureTooHigh = 2,
};

#endif
