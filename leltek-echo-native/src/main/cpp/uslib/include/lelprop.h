//
//  lelprop.h
//  Leltek UltraSound
//
//  Created by Leow on 2019/8/11.
//  Copyright © 2019 Leltek. All rights reserved.
//

#ifndef lelprop_h
#define lelprop_h

typedef enum
{
    i_START,            // Start integer props
    iScanMode,          // Set: scan mode(int). See ScanMode enum
    iRoiLinearPosition, // set Linear ROI position. Set: roiXPx, roiYPx, roiX2Px, roiY2Px(int array)
    iPreset,            // Set/Get: preset ID(int)

    /**
     * Set frame index or get valid frame length in review mode(B or C mode).
     * Get: valid frame length(int)(maximum is 100 by default)
     * Set: frame index(int)
     */
    iReview,

    /**
     * Set line index or get valid line length in PW review mode.
     * Get: valid line length(int)
     * Set: line index(int)
     */
    iReviewPw,

    // todo: iMirror absent

    iMaxThreadNum,      // set max thread number. Set: max thread number(int)
    iJpegEn,            // enable jpeg encode. Set/Get: 1 : enabled, 0 : disabled(int)
    iGrayMap,           // get gray map. Get: 256 element of gray map(int array)
    iColorMapR,         // get color map of Red. Get: 256 element of color map for Red(int array)
    iColorMapG,         // get color map of Green. Get: 256 element of color map for Green(int array)
    iColorMapB,         // get color map of Blue. Get: 256 element of color map for Blue(int array)
    iConnect,           // get tcp connection status. Get: 1 : connected, 0 : disconnected(int)
    iStartScan,         // get scan status. Get: 1 : scan started, 0 :scan stopped(int)
    iPwReverse,         // Set/Get: 1 : reverse, 0 : not reverse
    iPwEnvelopeLine,    // todo ??
    iPowerCmode,        // PD mode. Set/Get: 1 : colorDoppler; 2 : powerDoppler; 6 : dirPowerDoppler
    iDisableInternalPW, // disable internal PW to use external vendor PW. Set/Get: 1 : disabled, 0 : enabled
    iDisableInternalPP, // disable internal Post Processor to use external vendor PP. Set/Get: 1 : disabled, 0 : enabled
    iTHI,               // enable/disable THI. Set/Get: 1 : enabled, 0 : disabled
    iSmoothBuffer,      // enable/disable smooth buffer. Set/Get: 1 : enabled make image displayed more smoothly but causesome latency or delay; 0 : disabled

    /**
     * Determine the data type for LelGetRawBBuffer() and LelGetRawCBuffer().
     * Set/Get: 0: the data is fetched before post processor; 1: the data is fetched after post processor
     */
    iRawModeType,

    // todo: fRoiPercentage absent

    iFanMode,  // todo: ??
    iUniqueId, // hw id
    i_END,     // End integer props

    f_START,            // Start float props
    fRoiConvexPosition, // set Convex ROI position. Set: roiStartRPx, roiEndRPx, roiStartTheta, roiEndTheta(float array)
    fMModeX,            // set X position in M mode. Set: X position(float)
    fTgc,               // set TGC values. Set: TGC values(float array)
    fPwModeX,           // set X position in PW mode. Set: X position(float)
    fPwModeY,           // set Y position in PW mode. Set:Y position(float)
    fPwMaxSpeed,        // get Max speed in PW mode. Get: Max speed(float)
    fCMaxFlowSpeed,     // get Max flow speed in C mode. Get: Max speed(float)
    f_END               // End float props
} PropId;

typedef enum
{
    MODE_B,
    MODE_C,
    MODE_M,
    MODE_PW,
} ScanMode;

typedef enum
{
    dev_virtual,
    dev_wifi,
    dev_usb,
} DeviceType;

#endif /* lelprop_h */
