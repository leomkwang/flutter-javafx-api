#ifndef ANDROID_LOGPRINT_H
#define ANDROID_LOGPRINT_H

#if defined(__ANDROID__)
#include <android/log.h>
#define  LOGD(...) __android_log_print(ANDROID_LOG_DEBUG,"Debug" ,__VA_ARGS__)
#define  LOGI(...) __android_log_print(ANDROID_LOG_INFO,"Info" ,__VA_ARGS__)
#define  LOGE(...) __android_log_print(ANDROID_LOG_ERROR,"Error" ,__VA_ARGS__)
#else
#define  LOGD printf
#define  LOGI printf
#define  LOGE printf
#endif

#define D  printf


#endif //ANDROID_LOGPRINT_H
