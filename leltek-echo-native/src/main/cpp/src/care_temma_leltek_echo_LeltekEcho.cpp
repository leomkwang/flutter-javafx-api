#include "care_temma_leltek_echo_LeltekEcho.h"
#include <jni.h>

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <stdint.h>
#include <pthread.h>

#include <dirent.h>
#include "LogPrint.h"
#include "Leltek UltraSound-Bridging-Header.h"
//#include "vendorImp.h"

#ifdef __cplusplus
extern "C" {
#endif

JavaVM *g_jvm = NULL;
jobject g_obj;
pthread_key_t g_key;


void onThreadExit(void* tlsData) {
    JNIEnv* env = (JNIEnv*)tlsData;
    // Do some JNI calls with env if needed ...
    g_jvm->DetachCurrentThread();
}


JNIEnv* getJNIEnv(JavaVM* vm) {
    JNIEnv *env = (JNIEnv *) pthread_getspecific(g_key);  // gKey created by pthread_key_create() before
    if (NULL == env) {
        if (JNI_OK != vm->GetEnv(reinterpret_cast<void **>(&env), JNI_VERSION_1_6)) {
            if (JNI_OK == vm->AttachCurrentThread(reinterpret_cast<void **>(&env), NULL)) {
                pthread_setspecific(g_key, env); // Save JNIEnv* to TLS with gKey
            }
        }
    }
    return env;
}


void onEvent(EventId eventID, int iParam, float fParam)
{
    printf("onEvent %d\n", eventID);

    if (g_jvm == NULL)
        return;

    JNIEnv *env = getJNIEnv(g_jvm);
    if (env == NULL || g_obj == NULL)
        return;

    jclass cls = env->GetObjectClass(g_obj);
    jmethodID method = env->GetMethodID(cls, "onEventJNI", "(IIF)V");
    if (method == NULL)
        return;
/*
    if (eventID == ON_NEW_RAW_PW_READY) {
        VendorPwProcess(iParam);
        return;
    } else if (eventID == ON_NEW_RAW_B_READY) {
        VendorBProcess(iParam);
        return;
    } else if (eventID == ON_NEW_RAW_C_READY) {
        VendorCProcess(iParam);
        return;
    } else if (eventID == ON_GYRO_READY) {
        VendorGyroProcess();
        return;
    }
*/
    env->CallVoidMethod(g_obj, method, eventID, iParam, fParam);
}


JNIEXPORT void JNICALL Java_care_temma_leltek_echo_LeltekEcho_setJniRef(JNIEnv *env, jclass cls, jobject object_ls)
{
    env->GetJavaVM(&g_jvm);
    g_obj = env->NewGlobalRef(object_ls);

    pthread_key_create(&g_key, onThreadExit);
}


JNIEXPORT jboolean JNICALL Java_care_temma_leltek_echo_LeltekEcho_LelInitialize(JNIEnv *env, jclass cls)
{
    char path[1024];

    if (getenv("USCFG") == NULL)
        sprintf(path, "%s/usview", getenv("HOME"));
    else {
        printf("USCFG=%s\n", getenv("USCFG"));
        sprintf(path, "%s/usview", getenv("USCFG"));
    }

    printf("path=%s\n", path);

    return LelInitialize(path, onEvent);
}


JNIEXPORT jboolean JNICALL Java_care_temma_leltek_echo_LeltekEcho_LelConnect(JNIEnv *env, jclass cls, jint devType, jint param)
{
    return LelConnect(static_cast<DeviceType>(devType), param);
}


JNIEXPORT void JNICALL Java_care_temma_leltek_echo_LeltekEcho_LelDisconnect(JNIEnv *env, jclass cls)
{
    LelDisconnect();
}


JNIEXPORT jboolean JNICALL Java_care_temma_leltek_echo_LeltekEcho_LelInitProbe(JNIEnv *env, jclass cls)
{
    return LelInitProbe();
}


JNIEXPORT jboolean JNICALL Java_care_temma_leltek_echo_LeltekEcho_LelStartScan(JNIEnv *env, jclass obj)
{
    return LelStartScan();
}


JNIEXPORT void JNICALL Java_care_temma_leltek_echo_LeltekEcho_LelStopScan(JNIEnv *env,jclass obj)
{
    LelStopScan();
}


JNIEXPORT jboolean JNICALL Java_care_temma_leltek_echo_LeltekEcho_LelSetPropInt(JNIEnv *env, jclass cls, jint id, jint data)
{
    int tmp = data;
    return LelSetProp((PropId)id, &tmp);
}


JNIEXPORT jint JNICALL Java_care_temma_leltek_echo_LeltekEcho_LelGetPropInt(JNIEnv *env, jclass cls, jint id)
{
    int tmp = 0;
    LelGetProp((PropId)id, &tmp);
    return tmp;
}


JNIEXPORT jboolean JNICALL Java_care_temma_leltek_echo_LeltekEcho_LelSetPropFloat(JNIEnv *env, jclass cls, jint id, jfloat data)
{
    float tmp = data;
    return LelSetProp((PropId)id, &tmp);
}


JNIEXPORT jfloat JNICALL Java_care_temma_leltek_echo_LeltekEcho_LelGetPropFloat(JNIEnv *env, jclass cls, jint id)
{
    float tmp = 0;
    LelGetProp((PropId)id, &tmp);
    return tmp;
}


JNIEXPORT jboolean JNICALL Java_care_temma_leltek_echo_LeltekEcho_LelSetPropArrayInt(JNIEnv *env, jclass cls, jint id, jintArray array)
{
    jboolean b;
    int len = env->GetArrayLength(array);
    int* data = env->GetIntArrayElements(array, &b);
    return LelSetPropArray((PropId)id, data, len);
}


JNIEXPORT jboolean JNICALL Java_care_temma_leltek_echo_LeltekEcho_LelSetPropArrayFloat(JNIEnv *env, jclass cls, jint id, jfloatArray array)
{
    jboolean b;
    int len = env->GetArrayLength(array);
    float* data = env->GetFloatArrayElements(array, &b);
    return LelSetPropArray((PropId)id, data, len);
}


JNIEXPORT jbyteArray JNICALL Java_care_temma_leltek_echo_LeltekEcho_LelGetImageBuffer(JNIEnv *env, jclass cls, jint len)
{
    unsigned char *img = LelGetImageBuffer();
    if ( img == NULL )
        return NULL;
    jbyteArray array = env->NewByteArray(len);
    env->SetByteArrayRegion(array, 0, len, reinterpret_cast<jbyte*>(img));
    return array;
}


JNIEXPORT jbyteArray JNICALL Java_care_temma_leltek_echo_LeltekEcho_LelGetImageMBuffer(JNIEnv *env, jclass cls, jint len)
{
    unsigned char *img = LelGetImageMBuffer();
    if ( img == NULL )
        return NULL;
    jbyteArray array = env->NewByteArray(len);
    env->SetByteArrayRegion(array, 0, len, reinterpret_cast<jbyte*>(img));
    return array;
}


JNIEXPORT jbyteArray JNICALL Java_care_temma_leltek_echo_LeltekEcho_LelGetImagePWBuffer(JNIEnv *env, jclass cls, jint len)
{
    unsigned char *img = LelGetImagePWBuffer();
    if ( img == NULL )
        return NULL;
    jbyteArray array = env->NewByteArray(len);
    env->SetByteArrayRegion(array, 0, len, reinterpret_cast<jbyte*>(img));
    return array;
}


JNIEXPORT jbyteArray JNICALL Java_care_temma_leltek_echo_LeltekEcho_LelGetAudioBuffer(JNIEnv *env, jclass cls, jint len)
{
    unsigned char *img = LelGetAudioBuffer();
    if ( img == NULL )
        return NULL;
    jbyteArray array = env->NewByteArray(len);
    env->SetByteArrayRegion(array, 0, len, reinterpret_cast<jbyte*>(img));
    return array;
}


JNIEXPORT jstring JNICALL Java_care_temma_leltek_echo_LeltekEcho_LelMethod(JNIEnv *env, jclass cls, jstring method, jstring input)
{
    char output[1024*10];
    jboolean b = LelInvokeMethod(env-> GetStringUTFChars(method, 0), env-> GetStringUTFChars(input, 0), output);
    if (b) {
        jstring out = env->NewStringUTF(output);
        return out;
    } else
        return nullptr;
}


JNIEXPORT jintArray JNICALL Java_care_temma_leltek_echo_LeltekEcho_LelGetPropArrayInt(JNIEnv *env, jclass cls, jint id)
{
    int len = LelGetPropArray((PropId)id, NULL);
    int* data = new int[len];
    LelGetPropArray((PropId)id, data);
    jintArray array = env->NewIntArray(len);
    env->SetIntArrayRegion(array, 0, len, data);
    delete [] data;
    return array;
}


JNIEXPORT jfloatArray JNICALL Java_care_temma_leltek_echo_LeltekEcho_LelGetPropArrayFloat(JNIEnv *env, jclass cls, jint id)
{
    int len = LelGetPropArray((PropId)id, NULL);
    float* data = new float[len];
    LelGetPropArray((PropId)id, data);
    jfloatArray array = env->NewFloatArray(len);
    env->SetFloatArrayRegion(array, 0, len, data);
    delete [] data;
    return array;
}


JNIEXPORT jint JNICALL Java_care_temma_leltek_echo_LeltekEcho_LelGetErrorCode(JNIEnv *env, jclass cls)
{
    return LelGetErrorCode();
}


JNIEXPORT void JNICALL Java_care_temma_leltek_echo_LeltekEcho_LelSetPWRealTimeDrawLineDetails(JNIEnv *env, jclass cls, jint colorUp, jint colorDown, jshort thickness,jint colorCrest,jint colorTrough)
{
    LelSetPWRealTimeDrawLineDetails(colorUp, colorDown, thickness, colorCrest, colorTrough);
}


JNIEXPORT jboolean JNICALL Java_care_temma_leltek_echo_LeltekEcho_LelEnumUsb(JNIEnv *env, jclass cls)
{
    return LelEnumUsb();
}

/**
 * TODO: NOT USED
 * @param env
 * @param obj
 */
void Java_com_leltek_lelusviewer_LelJNI_LelTermination( JNIEnv *env, jobject obj)
{
    LelDestruct ();
    env->DeleteGlobalRef(g_obj);
    g_obj = NULL;
}


#ifdef __cplusplus
}
#endif
