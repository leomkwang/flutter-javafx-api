package care.temma.leltek.echo;


interface LeltekProbeListener {

    void onProbeEvent(int eventId, int param1, float param2);

}
