package care.temma.leltek.echo;

import java.nio.file.FileSystems;


public class LeltekEcho {

    static {
        System.load(
                FileSystems.getDefault()
                        .getPath("/etc/webrtc/libleltek_echo_native.so")
                        .normalize().toAbsolutePath().toString());
    }

    public static final int ON_NEW_IMAGE_READY    = 0; // B mode image ready. iParam: size in bytes
    public static final int ON_NEW_IMAGE_M_READY  = 1; // M mode image ready. iParam: size in bytes
    public static final int ON_NEW_IMAGE_PW_READY = 2; // PW mode image ready. iParam: size in bytes
    public static final int ON_NEW_AUDIO_READY    = 3; // todo: PW audio data ready. iParam: audio length? or sample rate?
    public static final int ON_NEW_RAW_PW_READY   = 4; // PW raw data (IQ) ready. iParam: data length
    public static final int ON_NEW_RAW_B_READY    = 5; // B mode raw data ready. iParam: data length (beamLine*512), beamLine is 128 or 256
    public static final int ON_NEW_RAW_C_READY    = 6; // C mode raw data ready. iParam: data length (128*256)
    public static final int ON_GSENSOR_RECEIVE    = 7; // G-sensor state notification. iParam: 0: none, 1: motion, 2: sleep
    public static final int ON_BATTERY_LEVEL_TOO_LOW_ERROR = 8; // Battery level too low notification
    public static final int ON_TEMPERATURE_OVERHEAT_ERROR  = 9; // Temperature overheat notification
    public static final int ON_CONNECTION     = 10; // LelConnect() result. iParam: 0 = fail, 1 = successful
    public static final int ON_DISCONNECTION  = 11; // Disconnection notification(TCP socket)
    public static final int ON_TCP_ERROR      = 12;
    public static final int ON_BUTTON         = 13; // Freeze button status. iParam: 0 = released, 1 = pressed
    public static final int ON_INIT_PROGRESS  = 14; // Report init progress in percentage. iParam: 0-100
    public static final int ON_INITED         = 15; // LelInitProbe() result. iParam: 0 = fail, 1 = successful
    public static final int ON_USB_CONNECTION = 16; // USB connect/disconnect event. iParam: 0 = connected, 1 = disconnected
    public static final int ON_GYRO_READY     = 17; // GYRO data ready event
    public static final int ON_USB_ERROR      = 18;

    public enum DeviceType {
        virtual,
        wifi,
        usb,
    }

    private static LeltekEcho lelJni;
    private LeltekProbeListener probeListener;

    static LeltekEcho getJniInstance() {
        if (lelJni == null) {
            lelJni = new LeltekEcho();
        }

        setJniRef(lelJni);
        return lelJni;
    }


    public void setListener(LeltekProbeListener probeListener) {
        this.probeListener = probeListener;
    }


    private void onEventJNI(int eventId, int iParam, float fParam) {
        probeListener.onProbeEvent(eventId, iParam, fParam);
    }


    private static native void setJniRef(Object cls);
    static public native boolean LelInitialize();
    static public native boolean LelConnect(int devType, int param);
    static public native void LelDisconnect();
    static public native boolean LelInitProbe();
    static public native boolean LelStartScan();
    static public native void LelStopScan();
    static public native boolean LelSetPropInt(int id, int data);
    static public native int LelGetPropInt(int id);
    static public native boolean LelSetPropFloat(int id, float data);
    static public native float LelGetPropFloat(int id);
    static public native boolean LelSetPropArrayInt(int id, int[] data);
    static public native boolean LelSetPropArrayFloat(int id, float[] data);
    static public native byte[] LelGetImageBuffer(int len);
    static public native byte[] LelGetImageMBuffer(int len);
    static public native byte[] LelGetImagePWBuffer(int len);
    static public native byte[] LelGetAudioBuffer(int len);
    static public native String LelMethod(String method, String input);
    static public native int[] LelGetPropArrayInt(int id);
    static public native float[] LelGetPropArrayFloat(int id);
    static public native int LelGetErrorCode();
    static public native void LelSetPWRealTimeDrawLineDetails(int colorUp, int colorDown, short thickness, int colorCrest, int colorTrough);
    static public native boolean LelEnumUsb();
}
