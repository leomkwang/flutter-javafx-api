package care.temma.leltek.echo;

import java.util.ArrayList;
import java.util.List;

public class ProbeParam {

    public final static String DB_COL_TXFREQ           = "TXFreq";
    public final static String DB_COL_ENHANCELEVEL     = "enhanceLevel";
    public final static String DB_COL_DEPTH            = "depth";
    public final static String DB_COL_FRAMERATE        = "frameRate";
    public final static String DB_COL_COMPOUNDNUM      = "compoundNum";
    public final static String DB_COL_CYCLENUM         = "cycleNum";
    public final static String DB_COL_PERSISTENCE      = "persistence";
    public final static String DB_COL_GAIN             = "gain";
    public final static String DB_COL_BEAMLINE         = "beamline";
    public final static String DB_COL_DR               = "dynamicRange";
    public final static String DB_COL_GRAYMAP          = "grayMap";
    public final static String DB_COL_BEAMFORMMETHOD   = "beamformMethod";
    public final static String DB_COL_VOLTAGE          = "voltage";
    public final static String DB_COL_TXFREQHARMONIC   = "TXFreqHarmonic";
    public final static String DB_COL_CYCLENUMBMODETHI = "cycleNumBmodeTHI";
    public final static String DB_COL_COLORPRF         = "colorPrf";
    public final static String DB_COL_COLORPOWERTHD    = "colorPowerThreshold";
    public final static String DB_COL_COLORBTHRESHOLD  = "colorBThreshold";
    public final static String DB_COL_GAINCONTROLCMODE = "gainControlCmode";
    public final static String DB_COL_MAGTHBYPWRCMODE  = "magThByPwrCmode";
    public final static String DB_COL_COLORANGLE       = "colorAngle";
    public final static String DB_COL_COLORWALLFILTER  = "colorWallFilter";
    public final static String DB_COL_MPRF             = "MPrf";
    public final static String DB_COL_PWGATE           = "gatePWmode";
    public final static String DB_COL_PWANGLE          = "PWAngle";
    public final static String DB_COL_PWPRF            = "PWPrf";
    public final static String DB_COL_PWBASELINE       = "PWBaseline";

    public int gain;
    public int depthIndex;
    public int frameRate;
    public int txFreq;
    public int dr;
    public int grayMap;
    public int enhancement;
    public int persistence;
    public int colorPrf;
    public int colorAngle;
    public int colorGain;
    public int colorNoise;
    public int colorWallFilter;
    public int pwPrf;
    public int pwGate;
    public int pwAngle;
    public int pwBaseline;
    public boolean pwReverse;
    public boolean pwEnvelopeLine;
    public int mPrf;
    public int powerCmode;
    public int beamformMethod;
    public boolean thi;
    public boolean smoothBuffer;

    public ScanMode scanMode;

    public List<Double> depthList = new ArrayList<>();
    public List<Integer> frameRateList = new ArrayList<>();
    public List<Double> txFreqList = new ArrayList<>();
    public List<Integer> grayMapList = new ArrayList<>();
    public List<Integer> colorMapRList = new ArrayList<>();
    public List<Integer> colorMapGList = new ArrayList<>();
    public List<Integer> colorMapBList = new ArrayList<>();
}