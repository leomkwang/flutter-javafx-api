package care.temma.leltek.echo;

import java.util.Set;

public enum ScanMode {
    MODE_B("b"),
    MODE_C("c"),
    MODE_M("m"),
    MODE_PW("pw");

    public final String mode;

    ScanMode(String mode) {
        this.mode = mode;
    }

    public static ScanMode getScanMode(String mode) throws Exception {
        if (ScanMode.MODE_B.mode.equals(mode)) {
            return ScanMode.MODE_B;
        }

        if (ScanMode.MODE_C.mode.equals(mode)) {
            return ScanMode.MODE_C;
        }

        if (ScanMode.MODE_M.mode.equals(mode)) {
            return ScanMode.MODE_M;
        }

        if (ScanMode.MODE_PW.mode.equals(mode)) {
            return ScanMode.MODE_PW;
        }

        throw new Exception("Unknown scan mode mode:" + mode);
    }

    /**
     * Get params available to set/get from the probe according to the current scan mode.
     * @return Set of ProbeParam parameters.
     * @see ProbeParam
     */
    public Set<String> getAvailableParams() {
        switch (this) {
            case MODE_B -> {
                return Set.of(
                        ProbeParam.DB_COL_DEPTH,
                        ProbeParam.DB_COL_TXFREQ,
                        ProbeParam.DB_COL_GAIN,
                        ProbeParam.DB_COL_DR,
                        ProbeParam.DB_COL_ENHANCELEVEL,
                        ProbeParam.DB_COL_GRAYMAP,
                        ProbeParam.DB_COL_PERSISTENCE,
                        ProbeParam.DB_COL_FRAMERATE,
                        ProbeParam.DB_COL_BEAMFORMMETHOD);
            }
            case MODE_C -> {
                return Set.of(
                        ProbeParam.DB_COL_DEPTH,
                        ProbeParam.DB_COL_TXFREQ,
                        ProbeParam.DB_COL_COLORPOWERTHD,
                        ProbeParam.DB_COL_COLORBTHRESHOLD,
                        ProbeParam.DB_COL_COLORANGLE,
                        ProbeParam.DB_COL_COLORWALLFILTER,
                        ProbeParam.DB_COL_COLORPRF);
            }
            case MODE_M -> {
                return Set.of(
                    ProbeParam.DB_COL_GAIN,
                    ProbeParam.DB_COL_MPRF);
            }
            case MODE_PW -> {
                return Set.of(
                        ProbeParam.DB_COL_GAIN,
                        ProbeParam.DB_COL_PWGATE,
                        ProbeParam.DB_COL_PWANGLE,
                        ProbeParam.DB_COL_PWPRF,
                        ProbeParam.DB_COL_PWBASELINE);
            }
        }

        return Set.of();
    }

}
