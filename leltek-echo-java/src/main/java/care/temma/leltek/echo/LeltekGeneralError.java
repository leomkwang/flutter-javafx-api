package care.temma.leltek.echo;

public class LeltekGeneralError {

    public enum Cause {
        ON_TEMPERATURE_OVERHEAT_ERROR
    }

    public final Cause cause;

    public LeltekGeneralError(Cause cause) {
        this.cause = cause;
    }

}
