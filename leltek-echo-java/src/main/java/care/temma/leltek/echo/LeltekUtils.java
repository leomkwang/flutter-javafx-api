package care.temma.leltek.echo;

public class LeltekUtils {

    /**
     * ARGB to BGRA
     * @param bytes ARGB Image
     */
    public static void argbToBgra(byte[] bytes) {
        for (int i = 0; i < bytes.length / 4; i++) {
            byte a = bytes[i * 4    ];
            byte r = bytes[i * 4 + 1];
            byte g = bytes[i * 4 + 2];
            byte b = bytes[i * 4 + 3];
            bytes[i * 4    ] = b;
            bytes[i * 4 + 1] = g;
            bytes[i * 4 + 2] = r;
            bytes[i * 4 + 3] = a;
        }
    }

}