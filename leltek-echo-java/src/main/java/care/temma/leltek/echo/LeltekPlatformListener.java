package care.temma.leltek.echo;

import java.util.List;

public interface LeltekPlatformListener {

    void onLeltekProbeInited();

    void onLeltekProbeInfoUpdated(ProbeInfo info, ProbeParam param);

    void onLeltekProbePresetsUpdated(List<LeltekPreset> presetList, LeltekPreset.Preset currentPreset);

    /**
     * 4 bytes per pix, format: B:G:R:A
     * @param img
     */
    void onLeltekProbeImage(byte[] img);

    void onLeltekProbeMImage(byte[] img);

    void onLeltekProbePWImage(byte[] img);

    void onLeltekError(LeltekGeneralError error);

    void onLeltekError(LeltekInternalError error);

}