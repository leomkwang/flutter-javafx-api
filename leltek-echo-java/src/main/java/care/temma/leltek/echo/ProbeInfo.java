package care.temma.leltek.echo;


public class ProbeInfo {

    public int    probeId = 0;
    public int    maxThreadNum = 0;
    public double depth = 0;
    public double RPx = 0;
    public double originXPx = 0;
    public double originYPx = 0;
    public double theta = 0;
    public int    imageSizeW = 0;
    public int    imageSizeH = 0;
    public double mi = 0;
    public double ti = 0;
    public double fMModeX = 0;
    public double pwMaxSpeed = 0;
    public double cMaxFlowSpeed = 0;
    public int    imgPWH = 0;
    public int    imgPWW = 0;
    public int    imgMH = 0;
    public int    imgMW = 0;
    public int    fpgaRev = 0;
    public String clibVer = null;
    public int    fanMode = 0;

}