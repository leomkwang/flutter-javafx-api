package care.temma.leltek.echo;

import java.util.Map;

public class LeltekPreset {

    public enum Preset {
        ABDOMEN(0),
        ABDOMEN_DIFFICULT(1),
        RENAL(3),
        GYN(5),
        OB_MID_LATE(6),
        OB_EARLY(8),
        PERIPHERAL_VESSELS(11),
        THYROID(12),
        BREAST(13),
        SUPERFICIAL(14),
        MSK(15),
        CAROTID(17),
        SPLEEN(25),
        BIOPSY(90),
        PHANTOM(99);

        public final int id;

        Preset(int id) {
            this.id = id;
        }
    }

    private static final System.Logger LOGGER = System.getLogger(LeltekPreset.class.getName());

    public final Preset preset;
    private final int id;
    private final int parentID;
    private final String english;
    public final String icon;
    private final boolean isUser;


    public LeltekPreset(Map<String, String> result) throws Exception {
        id = Integer.parseInt(result.get("id"));
        parentID = Integer.parseInt(result.get("parentID"));
        english = result.get("english");
        icon = result.get("icon");
        isUser = Integer.parseInt(result.get("user")) != 0;
        preset = getPreset(id);

        LOGGER.log(System.Logger.Level.INFO, "LeltekPreset(id=" + id + ", parentID=" + parentID + ", english=" + english + ", icon=" + icon + ", isUser=" + (isUser ? "true" : "false"));
    }


    /**
     * Get preset by id.
     * @param id
     * @return Preset
     * @throws Exception if id is unknown.
     */
    public static Preset getPreset(int id) throws Exception {
        if (Preset.ABDOMEN.id == id) {
            return Preset.ABDOMEN;
        }

        if (Preset.ABDOMEN_DIFFICULT.id == id) {
            return Preset.ABDOMEN_DIFFICULT;
        }

        if (Preset.RENAL.id == id) {
            return Preset.RENAL;
        }

        if (Preset.GYN.id == id) {
            return Preset.GYN;
        }

        if (Preset.OB_MID_LATE.id == id) {
            return Preset.OB_MID_LATE;
        }

        if (Preset.OB_EARLY.id == id) {
            return Preset.OB_EARLY;
        }

        if (Preset.PERIPHERAL_VESSELS.id == id) {
            return Preset.PERIPHERAL_VESSELS;
        }

        if (Preset.THYROID.id == id) {
            return Preset.THYROID;
        }

        if (Preset.BREAST.id == id) {
            return Preset.BREAST;
        }

        if (Preset.SUPERFICIAL.id == id) {
            return Preset.SUPERFICIAL;
        }

        if (Preset.MSK.id == id) {
            return Preset.MSK;
        }

        if (Preset.CAROTID.id == id) {
            return Preset.CAROTID;
        }

        if (Preset.SPLEEN.id == id) {
            return Preset.SPLEEN;
        }

        if (Preset.PHANTOM.id == id) {
            return Preset.PHANTOM;
        }

        if (Preset.BIOPSY.id == id) {
            return Preset.BIOPSY;
        }

        throw new Exception("Unknown preset id:" + id);
    }

}
