package care.temma.leltek.echo;

import java.util.Map;

public class LeltekInternalError {

    public String func;
    public String msg;
    public int majorErrCode;
    public int minorErrCode;

    public LeltekInternalError(Map<String, String> result) {
        func = result.get("func");
        msg = result.get("msg");
        majorErrCode = Integer.parseInt(result.get("majorErrCode"));
        minorErrCode = Integer.parseInt(result.get("minorErrCode"));
    }

}