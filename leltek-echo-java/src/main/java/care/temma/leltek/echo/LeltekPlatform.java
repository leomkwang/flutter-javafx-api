package care.temma.leltek.echo;

import java.util.*;

import java.awt.Point;


public class LeltekPlatform implements LeltekProbeListener {

    private static final System.Logger LOGGER = System.getLogger(LeltekPlatform.class.getName());

    public enum PropId {
        i_START,            // Start integer props
        iScanMode,          // Set: scan mode(int). See ScanMode enum
        iRoiLinearPosition, // set Linear ROI position. Set: roiXPx, roiYPx, roiX2Px, roiY2Px(int array)
        iPreset,            // Set/Get: preset ID(int)

        /**
         * Set frame index or get valid frame length in review mode(B or C mode).
         * Get: valid frame length(int)(maximum is 100 by default)
         * Set: frame index(int)
         */
        iReview,

        /**
         * Set line index or get valid line length in PW review mode.
         * Get: valid line length(int)
         * Set: line index(int)
         */
        iReviewPw,

        // todo: iMirror absent

        iMaxThreadNum,      // set max thread number. Set: max thread number(int)
        iJpegEn,            // enable jpeg encode. Set/Get: 1 : enabled, 0 : disabled(int)
        iGrayMap,           // get gray map. Get: 256 element of gray map(int array)
        iColorMapR,         // get color map of Red. Get: 256 element of color map for Red(int array)
        iColorMapG,         // get color map of Green. Get: 256 element of color map for Green(int array)
        iColorMapB,         // get color map of Blue. Get: 256 element of color map for Blue(int array)
        iConnect,           // get tcp connection status. Get: 1 : connected, 0 : disconnected(int)
        iStartScan,         // get scan status. Get: 1 : scan started, 0 :scan stopped(int)
        iPwReverse,         // Set/Get: 1 : reverse, 0 : not reverse
        iPwEnvelopeLine,    // todo ??
        iPowerCmode,        // PD mode. Set/Get: 1 : colorDoppler; 2 : powerDoppler; 6 : dirPowerDoppler
        iDisableInternalPW, // disable internal PW to use external vendor PW. Set/Get: 1 : disabled, 0 : enabled
        iDisableInternalPP, // disable internal Post Processor to use external vendor PP. Set/Get: 1 : disabled, 0 : enabled
        iTHI,               // enable/disable THI. Set/Get: 1 : enabled, 0 : disabled
        iSmoothBuffer,      // enable/disable smooth buffer. Set/Get: 1 : enabled make image displayed more smoothly but causesome latency or delay; 0 : disabled

        /**
         * Determine the data type for LelGetRawBBuffer() and LelGetRawCBuffer().
         * Set/Get: 0: the data is fetched before post processor; 1: the data is fetched after post processor
         */
        iRawModeType,

        // todo: fRoiPercentage absent

        iFanMode,           // todo: ??
        iUniqueId,          // hw id
        i_END,              // End integer props

        f_START,            // Start float props
        fRoiConvexPosition, // set Convex ROI position. Set: roiStartRPx, roiEndRPx, roiStartTheta, roiEndTheta(float array)
        fMModeX,            // set X position in M mode. Set: X position(float)
        fTgc,               // set TGC values. Set: TGC values(float array)
        fPwModeX,           // set X position in PW mode. Set: X position(float)
        fPwModeY,           // set Y position in PW mode. Set:Y position(float)
        fPwMaxSpeed,        // get Max speed in PW mode. Get: Max speed(float)
        fCMaxFlowSpeed,     // get Max flow speed in C mode. Get: Max speed(float)
        f_END               // End float props
    }

    static class BoardInfo {
        boolean isBatteryCharging;
        double batteryRemaining;
        double temperature;
    }

    static class ImageInfo {
        int frameRate;
        int dataRate;
    }

    private static LeltekEcho jni;
    private LeltekPlatformListener listener;
    private String input;
    private boolean connected = false;

    public ProbeInfo probeInfo = new ProbeInfo();
    public ProbeParam probeParam = new ProbeParam();
    public BoardInfo boardInfo = new BoardInfo();
    public ImageInfo imageInfo = new ImageInfo();

    public List<LeltekPreset> presetList = new ArrayList<>();
    public LeltekPreset.Preset preset;

    Point convexOrigin = new Point();

    public LeltekPlatform() {
        init();
    }


    public void setListener(LeltekPlatformListener listener) {
        this.listener = listener;
    }


    /**
     *
     */
    public void init() {
        connected = false;
        jni = LeltekEcho.getJniInstance();
        jni.setListener(this);
        jni.LelInitialize();
    }


    @Override
    public void onProbeEvent(int eventId, int iParam, float fParam) {
        switch (eventId) {
            case LeltekEcho.ON_CONNECTION -> {
                LOGGER.log(System.Logger.Level.INFO, "ON_CONNECTION");
                if (iParam == 1) {
                  connected = true;
                  initProbe();
                } else {
                  LOGGER.log(System.Logger.Level.INFO, "LelConnect failed");
                  listener.onLeltekError(getErrInfo());
                }
            }

            case LeltekEcho.ON_INITED -> {
                LOGGER.log(System.Logger.Level.INFO, "ON_INITED " + (iParam == 1 ? "ok" : "ko"));

                if (iParam == 1) {
                    setScanMode(ScanMode.MODE_B);
                    getProbeInfo();
                    getPresets();
                    listener.onLeltekProbeInited();
                    startScan();
                } else {
                    listener.onLeltekError(getErrInfo());
                }
            }

            case LeltekEcho.ON_NEW_IMAGE_READY -> {
                LOGGER.log(System.Logger.Level.INFO, "ON_NEW_IMAGE_READY size=" + iParam);
                // 4 bytes per pix, format: A:R:G:B
                listener.onLeltekProbeImage(getImageBuffer(iParam));
            }

            case LeltekEcho.ON_NEW_IMAGE_M_READY -> {
                LOGGER.log(System.Logger.Level.INFO, "ON_NEW_IMAGE_M_READY size=" + iParam);
                listener.onLeltekProbeMImage(getImageMBuffer(iParam));
            }

            case LeltekEcho.ON_NEW_IMAGE_PW_READY -> {
                LOGGER.log(System.Logger.Level.INFO, "ON_NEW_IMAGE_PW_READY size=" + iParam);
                listener.onLeltekProbePWImage(getImagePWBuffer(iParam));
            }

            case LeltekEcho.ON_NEW_AUDIO_READY -> {
                LOGGER.log(System.Logger.Level.INFO, "ON_NEW_AUDIO_READY");
            }

            case LeltekEcho.ON_NEW_RAW_PW_READY -> {
                LOGGER.log(System.Logger.Level.INFO, "ON_NEW_RAW_PW_READY size=" + iParam);
            }

            case LeltekEcho.ON_NEW_RAW_B_READY -> {
                LOGGER.log(System.Logger.Level.INFO, "ON_NEW_RAW_B_READY size=" + iParam);
                listener.onLeltekProbeImage(getImageBuffer(iParam));
            }

            case LeltekEcho.ON_NEW_RAW_C_READY -> {
                LOGGER.log(System.Logger.Level.INFO, "ON_NEW_RAW_C_READY size=" + iParam);
            }

            case LeltekEcho.ON_GSENSOR_RECEIVE -> {
                LOGGER.log(System.Logger.Level.INFO, "ON_GSENSOR_RECEIVE");
            }

            case LeltekEcho.ON_BATTERY_LEVEL_TOO_LOW_ERROR -> {
                LOGGER.log(System.Logger.Level.INFO, "ON_BATTERY_LEVEL_TOO_LOW_ERROR");
            }

            case LeltekEcho.ON_TEMPERATURE_OVERHEAT_ERROR -> {
                listener.onLeltekError(new LeltekGeneralError(LeltekGeneralError.Cause.ON_TEMPERATURE_OVERHEAT_ERROR));
            }

            case LeltekEcho.ON_DISCONNECTION -> {
                LOGGER.log(System.Logger.Level.INFO, "ON_DISCONNECTION");
            }

            case LeltekEcho.ON_TCP_ERROR -> {
                LOGGER.log(System.Logger.Level.INFO, "ON_TCP_ERROR");
            }

            case LeltekEcho.ON_BUTTON -> {
                LOGGER.log(System.Logger.Level.INFO, "ON_BUTTON");
            }

            case LeltekEcho.ON_INIT_PROGRESS -> {
                LOGGER.log(System.Logger.Level.INFO, "ON_INIT_PROGRESS " + iParam);
            }

            case LeltekEcho.ON_USB_CONNECTION -> {
                LOGGER.log(System.Logger.Level.INFO, "ON_USB_CONNECTION");
            }

            case LeltekEcho.ON_GYRO_READY -> {
                LOGGER.log(System.Logger.Level.INFO, "ON_GYRO_READY");
            }

            case LeltekEcho.ON_USB_ERROR -> {
                LOGGER.log(System.Logger.Level.INFO, "ON_USB_ERROR");
                listener.onLeltekError(getErrInfo());
            }
        }
    }


    public boolean enumUsb() {
        return jni.LelEnumUsb();
    }


    public boolean connect(LeltekEcho.DeviceType devType, int param) {
        return jni.LelConnect(devType.ordinal(), param);
    }


    public void disconnect() {
        if (connected) {
            jni.LelDisconnect();
            connected = false;
        }
    }


    private void initProbe() {
        jni.LelInitProbe();
    }


    /**
     * Start scan.
     */
    public void startScan() {
        jni.LelStartScan();
    }


    /**
     * Stop scan.
     */
    private void stopScan() {
        jni.LelStopScan();
    }


    /**
     * Get an image buffer.
     * @param size
     * @return Image buffer
     */
    public byte[] getImageBuffer(int size) {
        return jni.LelGetImageBuffer(size);
    }

    public byte[] getImageMBuffer(int size) {
        return jni.LelGetImageMBuffer(size);
    }

    public byte[] getImagePWBuffer(int size) {
        return jni.LelGetImagePWBuffer(size);
    }

    /**
     * Get the last error.
     * @return Error object or null.
     */
    private LeltekInternalError getErrInfo() {
        if (!connected) {
            return null;
        }

        Map<String, String> input = new HashMap<>();
        Map<String, String> result = invokeMethod("getErrInfo", input);
        if (result == null) {
            return null;
        }

        return new LeltekInternalError(result);
    }



    /**
     *
     * @param method
     * @param arguments
     * @return
     */
    private Map<String, String> invokeMethod(String method, Map<String, String> arguments) {
        input = "";
        arguments.forEach((key, value) -> {
            if (key.equals("method")) {
                return;
            }

            input += key + "=" + value + "\n";
        });

        String output = jni.LelMethod(method, input);
        if (output == null) {
            return null;
        }

        //LOGGER.log(System.Logger.Level.INFO, "@@output=" + output);

        Map<String, String> out = new HashMap<>();
        if (!output.isEmpty()) {
            for (String prop: output.split("\n")) {
                String[] kv = prop.split("=");
                String key = kv[0].trim();
                String value = kv[1].trim();
                out.put(key, value);
            }
        }

        return out;
    }



    /**
     * Update probe info.
     */
    private void getProbeInfo() {
        Map<String, String> input = new HashMap<>();
        Map<String, String> result = invokeMethod("getProbeInfo", input);
        if (result == null) {
            return;
        }

        probeInfo.probeId = Integer.parseInt(result.get("probeId"));
        probeInfo.maxThreadNum = 0;
        probeInfo.depth = Double.parseDouble(result.get("depth"));
        probeInfo.RPx = Double.parseDouble(result.get("RPx"));
        probeInfo.originXPx = Double.parseDouble(result.get("originXPx"));
        probeInfo.originYPx = Double.parseDouble(result.get("originYPx"));
        probeInfo.theta = Double.parseDouble(result.get("theta"));
        probeInfo.imageSizeW = Integer.parseInt(result.get("imgW"));
        probeInfo.imageSizeH = Integer.parseInt(result.get("imgH"));
        probeInfo.mi = Double.parseDouble(result.get("Mi"));
        probeInfo.ti = Double.parseDouble(result.get("Ti"));
        probeInfo.imgPWH = Integer.parseInt(result.get("imgPWH"));
        probeInfo.imgPWW = Integer.parseInt(result.get("imgPWW"));
        probeInfo.imgMH = Integer.parseInt(result.get("imgMH"));
        probeInfo.imgMW = Integer.parseInt(result.get("imgMW"));
        probeInfo.fpgaRev = Integer.parseInt(result.get("fpgaRev"));
        probeInfo.clibVer = result.get("clibVer");

        convexOrigin.x = (int)probeInfo.originXPx;
        convexOrigin.y = (int)probeInfo.originYPx;

        getProp(PropId.iJpegEn);
        getProp(PropId.iMaxThreadNum);
        getProp(PropId.iSmoothBuffer);
        getProp(PropId.iFanMode);

        switch (probeParam.scanMode) {
            case MODE_B -> getBModeParams();
            case MODE_C -> getCModeParams();
            case MODE_M -> getMModeParams();
            case MODE_PW -> getPwModeParams();
        }

        listener.onLeltekProbeInfoUpdated(probeInfo, probeParam);
    }


    /************
     * PROPERTIES
     ************/

    private boolean setProp(PropId id, Object data) {
        return id.ordinal() < PropId.i_END.ordinal()
                ? jni.LelSetPropInt(id.ordinal(), (Integer)data)
                : jni.LelSetPropFloat(id.ordinal(), (Float)data);
    }


    private void getProp(PropId id) {
        if (id.ordinal() < PropId.i_END.ordinal())
            getPropInt(id);
        else
            getPropFloat(id);
    }


    private void getPropInt(PropId id) {

        int result = jni.LelGetPropInt(id.ordinal());

        switch (id) {
          case iJpegEn:
            //imageInfo.jpegEn = (result != 0);
            //print('jpegEn=${imageInfo.jpegEn}');
            break;
          case iMaxThreadNum:
            probeInfo.maxThreadNum = result;
            //print('maxThreadNum=${probeInfo.maxThreadNum}');
            break;
          case iPwReverse:
            probeParam.pwReverse = (result != 0);
            //print('pwReverse=${probeParam.pwReverse}');
            break;
          case iPwEnvelopeLine:
            probeParam.pwEnvelopeLine = (result != 0);
            //print('iPwEnvelopeLine=${probeParam.pwEnvelopeLine}');
            break;
          case iPowerCmode:
            probeParam.powerCmode = result;
            //rint('iPowerCmode=${probeParam.powerCmode}');
            break;
          case iTHI:
            probeParam.thi = (result != 0);
            //print('iTHI=${probeParam.thi}');
            break;
          case iSmoothBuffer:
            probeParam.smoothBuffer = (result != 0);
            //print('iSmoothBuffer=${probeParam.smoothBuffer}');
            break;
          case iFanMode:
            probeInfo.fanMode = result;
            //print('iFanMode=${probeInfo.fanMode}');
            break;
          default:
        }
    }

    private void getPropFloat(PropId id) {

        double result = jni.LelGetPropFloat(id.ordinal());

        switch (id) {
            case fMModeX -> probeInfo.fMModeX = result;
            case fPwMaxSpeed -> probeInfo.pwMaxSpeed = result;
            case fCMaxFlowSpeed -> probeInfo.cMaxFlowSpeed = result;
        }
    }

    private void getPropArrayInt(PropId id) {
        int[] dataInt = jni.LelGetPropArrayInt(id.ordinal());

        switch (id) {
            case iGrayMap -> {
                probeParam.grayMapList.clear();
                for (int value : dataInt) {
                    probeParam.grayMapList.add(value);
                }
            }
            case iColorMapR -> {
                probeParam.colorMapRList.clear();
                for (int value : dataInt) {
                    probeParam.colorMapRList.add(value);
                }
            }
            case iColorMapG -> {
                probeParam.colorMapGList.clear();
                for (int value : dataInt) {
                    probeParam.colorMapGList.add(value);
                }
            }
            case iColorMapB -> {
                probeParam.colorMapBList.clear();
                for (int value : dataInt) {
                    probeParam.colorMapBList.add(value);
                }
            }
        }
    }

    private void getPropArrayFloat(PropId id) {

    }

    private void getPropArray(PropId id) {
        if (id.ordinal() < PropId.i_END.ordinal())
            getPropArrayInt(id);
        else
            getPropArrayFloat(id);
    }

    /****************
     * END PROPERTIES
     ****************/


    /********
     * PARAMS
     ********/


    public boolean setParam(String name, int val) {
        Map<String, String> input = new HashMap<>();
        input.put("name", name);
        input.put("val", Integer.toString(val));
        Map<String, String> result = invokeMethod("setParam", input);

        if (result == null) {
            LOGGER.log(System.Logger.Level.ERROR, "setParam(" + name + ", " + val + ") = NOK");
            return false;
        }

        LOGGER.log(System.Logger.Level.INFO, "setParam(" + name + ", " + val + ") = OK");
        getProbeInfo();

        return true;
    }


    private boolean getParamTbl(String name) {
        Map<String, String> input = new HashMap<>();
        input.put("name", name);
        Map<String, String> result = invokeMethod("getParamTbl", input);
        if (result == null) {
            return false;
        }

        String tbl = result.get("tbl");

        switch (name) {
            case ProbeParam.DB_COL_DEPTH -> {
                probeParam.depthList.clear();
                for (String param: tbl.split(" ")) {
                    probeParam.depthList.add(Double.parseDouble(param));
                }
            }

            case ProbeParam.DB_COL_FRAMERATE -> {
                probeParam.frameRateList.clear();
                for (String param: tbl.split(" ")) {
                    probeParam.frameRateList.add(Integer.parseInt(param));
                }
            }

            case ProbeParam.DB_COL_TXFREQ -> {
                probeParam.txFreqList.clear();
                for (String param: tbl.split(" ")) {
                    probeParam.txFreqList.add(Double.parseDouble(param));
                }
            }
        }

        return true;
    }


    private void getBModeParams() {
        getParamTbl(ProbeParam.DB_COL_DEPTH);
        getParamTbl(ProbeParam.DB_COL_FRAMERATE);
        getParamTbl(ProbeParam.DB_COL_TXFREQ);

        getParam(ProbeParam.DB_COL_DEPTH);
        getParam(ProbeParam.DB_COL_DR);
        getParam(ProbeParam.DB_COL_GRAYMAP);
        getParam(ProbeParam.DB_COL_ENHANCELEVEL);
        getParam(ProbeParam.DB_COL_PERSISTENCE);
        getParam(ProbeParam.DB_COL_GAIN);
        getParam(ProbeParam.DB_COL_FRAMERATE);
        getParam(ProbeParam.DB_COL_TXFREQ);

        getProp(PropId.iTHI);
    }


    private void getCModeParams() {
        getParamTbl(ProbeParam.DB_COL_TXFREQ);
        getParam(ProbeParam.DB_COL_TXFREQ);
        getColorMap();
    }


    private void getColorMap() {
        getPropArray(PropId.iColorMapR);
        getPropArray(PropId.iColorMapG);
        getPropArray(PropId.iColorMapB);
    }


    private void getMModeParams() {
        getProp(PropId.fMModeX);
    }


    private void getPwModeParams() {
        getParamTbl(ProbeParam.DB_COL_TXFREQ);
        getParam(ProbeParam.DB_COL_TXFREQ);
    }


    private int getParam(String name) {
        Map<String, String> input = new HashMap<>();
        input.put("name", name);
        Map<String, String> result = invokeMethod("getParam", input);
        if (result == null)
            return -1;
        String value = result.get("val");

        switch (name) {
            case ProbeParam.DB_COL_GAIN:
              probeParam.gain = Integer.parseInt(value);
              break;
            case ProbeParam.DB_COL_TXFREQ:
              probeParam.txFreq = Integer.parseInt(value);
              break;
            case ProbeParam.DB_COL_DEPTH:
              probeParam.depthIndex = Integer.parseInt(value);
              break;
            case ProbeParam.DB_COL_FRAMERATE:
              probeParam.frameRate = Integer.parseInt(value);
              break;
            case ProbeParam.DB_COL_BEAMFORMMETHOD:
              probeParam.beamformMethod = Integer.parseInt(value);
              break;
            case ProbeParam.DB_COL_DR:
              probeParam.dr = Integer.parseInt(value);
              break;
            case ProbeParam.DB_COL_GRAYMAP:
              probeParam.grayMap = Integer.parseInt(value);
              getPropArray(PropId.iGrayMap);
              break;
            case ProbeParam.DB_COL_ENHANCELEVEL:
              probeParam.enhancement = Integer.parseInt(value);
              break;
            case ProbeParam.DB_COL_PERSISTENCE:
              probeParam.persistence = Integer.parseInt(value);
              break;
            case ProbeParam.DB_COL_COLORPRF:
              probeParam.colorPrf = Integer.parseInt(value);
              break;
            case ProbeParam.DB_COL_COLORANGLE:
              probeParam.colorAngle = Integer.parseInt(value);
              break;
            case ProbeParam.DB_COL_COLORPOWERTHD:
              probeParam.colorGain = Integer.parseInt(value);
              break;
            case ProbeParam.DB_COL_COLORBTHRESHOLD:
              probeParam.colorNoise = Integer.parseInt(value);
              break;
            case ProbeParam.DB_COL_COLORWALLFILTER:
              probeParam.colorWallFilter = Integer.parseInt(value);
              break;
            case ProbeParam.DB_COL_MPRF:
              probeParam.mPrf = Integer.parseInt(value);
              break;
            case ProbeParam.DB_COL_PWPRF:
              probeParam.pwPrf = Integer.parseInt(value);
              break;
            case ProbeParam.DB_COL_PWGATE:
              probeParam.pwGate = Integer.parseInt(value);
              break;
            case ProbeParam.DB_COL_PWANGLE:
              probeParam.pwAngle = Integer.parseInt(value);
              break;
            case ProbeParam.DB_COL_PWBASELINE:
              probeParam.pwBaseline = Integer.parseInt(value);
              break;
        }
        return Integer.parseInt(value);
    }

    /************
     * END PARAMS
     ************/



    public boolean setScanMode(ScanMode mode) {
        if (mode == ScanMode.MODE_PW) {
            setParam(ProbeParam.DB_COL_PWGATE, probeParam.pwGate);
            setParam(ProbeParam.DB_COL_PWANGLE, probeParam.pwAngle);
            //setPwDrawLineDetails();
        }

        boolean result = setProp(PropId.iScanMode, mode.ordinal());

        if (result) {
            probeParam.scanMode = mode;
            getProbeInfo();
            //_getProbeInfo().then((val) => onCommandResult(CommandId.setScanMode, val));
        }

        return result;
    }


    /**
     * Fetch and update preset list and current preset.
     * @return
     */
    private void getPresets() {
        // Fetch and update the list of presets.

        Map<String, String> input = new HashMap<>();
        input.put("index", Integer.toString(-1));
        Map<String, String> result = invokeMethod("getPreset", input);
        if (result == null) {
            return;
        }

        int len = Integer.parseInt(result.get("len"));

        LOGGER.log(System.Logger.Level.INFO, "getPresets len = " + len);

        presetList.clear();
        for (int i = 0; i < len; i++) {
            try {
                presetList.add(getPreset(i));
            } catch (Exception e) {
                LOGGER.log(System.Logger.Level.ERROR, e.getMessage(), e);
            }
        }

        presetList.sort(Comparator.comparingInt(p -> p.preset.id));

        // Fetch and update the current preset.

        int presetId = jni.LelGetPropInt(PropId.iPreset.ordinal());

        try {
            preset = LeltekPreset.getPreset(presetId);
            LOGGER.log(System.Logger.Level.INFO, "Current preset = " + preset);

            if (presetList.stream().noneMatch(leltekPreset -> leltekPreset.preset == preset)) {
                throw new Exception("Current preset does not match the list of presets");
            }
        } catch (Exception e) {
            LOGGER.log(System.Logger.Level.ERROR, e.getMessage(), e);
            return;
        }

        listener.onLeltekProbePresetsUpdated(presetList, preset);
    }


    /**
     * Get preset info by index.
     * @param index
     * @return
     * @throws Exception
     */
    private LeltekPreset getPreset(int index) throws Exception {
        Map<String, String> input = new HashMap<>();
        input.put("index", Integer.toString(index));
        Map<String, String> result = invokeMethod("getPreset", input);
        if (result == null) {
            throw new Exception("Cannot get preset with index = " + index);
        }

        return new LeltekPreset(result);
    }


    public boolean setPreset(LeltekPreset.Preset preset) {
        boolean success = jni.LelSetPropInt(PropId.iPreset.ordinal(), preset.id);
        LOGGER.log(System.Logger.Level.INFO, "setPreset " + preset + ": " + (success ? "OK" : "NOK"));

        if (!success) {
            return false;
        }

        getProbeInfo();
        getPresets();

        // TODO: getParamTbl(ParamName.DB_COL_TXFREQ);
        // TODO: getParam(ParamName.DB_COL_TXFREQ);

        return true;
    }

    private double calTheta(Point pnt) {
        double diffX = convexOrigin.x - pnt.x;
        double diffY = Math.abs(pnt.y - convexOrigin.y);
        return Math.atan(diffX / diffY);
    }

    public void onTap(Point pnt) {
        if (probeParam.scanMode == ScanMode.MODE_M) {
            double ratioX;
            if (probeInfo.RPx == 0)
                ratioX = pnt.x / probeInfo.imageSizeW;
            else
                ratioX = (1-calTheta(pnt)/probeInfo.theta)/2;
            setProp(PropId.fMModeX, ratioX);
        }


    }
}

/*
class TuningParam {
  static const String start = '',
      depth = 'Depth(cm)',
      gain = 'Gain(dB)',
      frameRate = 'Frame Rate(Hz)',
      mGain = 'Gain(%)',
      dr = 'Dynamic Range(dB)',
      freq = 'Freq(MHz)',
      enhancement = 'Enhancement',
      grayMap = 'Gray Map',
      persistence = 'Persistence',
      colorGain = 'Color Gain',
      colorPrf = 'Scale PRF',
      colorAngle = 'Color Angle(degree)',
      colorNoise = 'Color B-reject',
      colorWallFilter = 'Wall Filter',
      thi = "THI",
      pwGate = 'PW Gate',
      pwAngle = 'PW Angle',
      pwPrf = 'Prf(MHz)',
      tgc = "TGC",
      beamformMethod = 'Beamform',
      mPrf = "Prf(Hz)",
      pwBaseline = 'PW Baseline',
      pwReverse = "PW Reverse",
      pwEnvelopeLine = "PW Envelope Line",
      end = '';
}
*/
