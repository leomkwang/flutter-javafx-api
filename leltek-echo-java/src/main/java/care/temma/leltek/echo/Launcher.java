package care.temma.leltek.echo;

import java.util.List;
import java.util.stream.Collectors;


public class Launcher implements LeltekPlatformListener {

    private static LeltekPlatform platform;

    void dumpDepth() {
        List<Double> depthList = platform.probeParam.depthList;
        showMsg("depthList\n");
        for (int i=0; i < depthList.size(); i++)
            showMsg("depthList[" + i + "]=" + depthList.get(i));
        int depthIndx =  platform.probeParam.depthIndex;
        showMsg("depthIndx=" + depthIndx);
    }

    @Override
    public void onLeltekProbeInited() {
        dumpDepth();

        try {
            Thread.sleep(1000);
        } catch(Exception e) {
            e.printStackTrace();
        }

        // change depth
        platform.setParam(ProbeParam.DB_COL_TXFREQ, 1);

        platform.setScanMode(ScanMode.MODE_M);
    }

    @Override
    public void onLeltekProbeInfoUpdated(ProbeInfo info, ProbeParam param) {
        showMsg("onLeltekProbeInfoUpdated()");
        showMsg("probeInfo.probeId = " + info.probeId);
        showMsg("probeInfo.RPx = " + info.RPx);
        showMsg("probeInfo.originXPx = " + info.originXPx);
        showMsg("probeInfo.originYPx = " + info.originYPx);
        showMsg("probeInfo.theta = " + info.theta);
        showMsg("probeInfo.imageSizeW = " + info.imageSizeW);
        showMsg("probeInfo.imageSizeH = " + info.imageSizeH);
        showMsg("probeInfo.mi = " + info.mi);
        showMsg("probeInfo.ti = " + info.ti);
        showMsg("probeInfo.ti = " + info.pwMaxSpeed);
        showMsg("probeInfo.ti = " + info.cMaxFlowSpeed);
        showMsg("probeInfo.imgPWH = " + info.imgPWH);
        showMsg("probeInfo.imgPWW = " + info.imgPWW);
        showMsg("probeInfo.imgMH = " + info.imgMH);
        showMsg("probeInfo.imgMW = " + info.imgMW);
        showMsg("probeInfo.fpgaRev = " + info.fpgaRev);
        showMsg("probeInfo.clibVer = " + info.clibVer);
        showMsg("probeInfo.clibVer = " + info.fanMode);

        showMsg("param.gain = " + param.gain);
        showMsg("param.depthIndex = " + param.depthIndex);
        showMsg("param.frameRate = " + param.frameRate);
        showMsg("param.txFreq = " + param.txFreq);
        showMsg("param.dr = " + param.dr);
        showMsg("param.grayMap = " + param.grayMap);
        showMsg("param.enhancement = " + param.enhancement);
        showMsg("param.persistence = " + param.persistence);
        showMsg("param.colorPrf = " + param.colorPrf);
        showMsg("param.colorAngle = " + param.colorAngle);
        showMsg("param.colorGain = " + param.colorGain);
        showMsg("param.colorNoise = " + param.colorNoise);
        showMsg("param.colorWallFilter = " + param.colorWallFilter);
        showMsg("param.pwPrf = " + param.pwPrf);
        showMsg("param.pwGate = " + param.pwGate);
        showMsg("param.pwAngle = " + param.pwAngle);
        showMsg("param.pwBaseline = " + param.pwBaseline);
        showMsg("param.pwReverse = " + param.pwReverse);
        showMsg("param.pwEnvelopeLine = " + param.pwEnvelopeLine);
        showMsg("param.mPrf = " + param.mPrf);
        showMsg("param.powerCmode = " + param.powerCmode);
        showMsg("param.beamformMethod = " + param.beamformMethod);
        showMsg("param.thi = " + param.thi);
        showMsg("param.smoothBuffer = " + param.smoothBuffer);
        showMsg("param.depthList = " + param.depthList.stream().map(String::valueOf).collect(Collectors.joining(", ")));
        showMsg("param.frameRateList = " + param.frameRateList.stream().map(String::valueOf).collect(Collectors.joining(", ")));
        showMsg("param.grayMapList = " + param.grayMapList.stream().map(String::valueOf).collect(Collectors.joining(", ")));
        showMsg("param.colorMapRList = " + param.colorMapRList.stream().map(String::valueOf).collect(Collectors.joining(", ")));
        showMsg("param.colorMapGList = " + param.colorMapGList.stream().map(String::valueOf).collect(Collectors.joining(", ")));
        showMsg("param.colorMapBList = " + param.colorMapBList.stream().map(String::valueOf).collect(Collectors.joining(", ")));
    }

    @Override
    public void onLeltekProbePresetsUpdated(List<LeltekPreset> presetList, LeltekPreset.Preset currentPreset) {

    }

    @Override
    public void onLeltekProbeImage(byte[] img) {
        showMsg("onLeltekProbe Image()");
        for (int i=0; i < 4; i++) {
            showMsg("" + i + ":" + String.format("%02X", img[i]));
        }
    }

    @Override
    public void onLeltekProbeMImage(byte[] img) {

        showMsg("onLeltekProbe M Image()");
        for (int i=0; i<4; i++) {
            showMsg("" + i + ":" + String.format("%02X", img[i]));
        }

    }

    @Override
    public void onLeltekProbePWImage(byte[] img) {

        showMsg("onLeltekProbe PW Image()");
        for (int i=0; i<4; i++) {
            showMsg("" + i + ":" + String.format("%02X", img[i]));
        }

    }

    private final static Object lock = new Object();

    public static void main(String[] args) {
        Launcher mainActivity = new Launcher();
        platform = new LeltekPlatform();
        platform.setListener(mainActivity);
        showMsg("main()");

        showMsg("enumUsb() " +  platform.enumUsb());

        platform.connect(LeltekEcho.DeviceType.virtual, 0x47);
        //platform.connect(LeltekEcho.DeviceType.usb, 0);

        try {
            synchronized (lock) {
                lock.wait();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        platform.disconnect();

        showMsg("main() quit");
    }


    private static void showMsg(String msg) {
        System.out.println(msg);
    }


    @Override
    public void onLeltekError(LeltekInternalError error) {
        showMsg("**onLeltekError()");
        showMsg("errInfo.func = " + error.func);
        showMsg("errInfo.msg = " +  error.msg);

        synchronized (lock) {
            lock.notify();
        }
    }

    @Override
    public void onLeltekError(LeltekGeneralError error) {
        showMsg("**onLeltekError(" + error.cause + ")");
    }

}