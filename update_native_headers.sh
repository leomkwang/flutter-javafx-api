#!/bin/bash

javac -h leltek-echo-native/src/main/cpp/include/ \
  -d leltek-echo-java/target/generate-native-headers-classes \
  leltek-echo-java/src/main/java/care/temma/leltek/echo/LeltekEcho.java \
  leltek-echo-java/src/main/java/care/temma/leltek/echo/LeltekProbeListener.java
