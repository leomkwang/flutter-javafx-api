# Leltek echograph SDK 

## Installation

#### OpenCV

Download [opencv-4.5.2](https://github.com/opencv/opencv/archive/refs/tags/4.5.2.zip).

Download [opencv_contrib-4.5.2](https://github.com/opencv/opencv_contrib/archive/refs/tags/4.5.2.zip) and place it to 
opencv-4.5.2 directory (`opencv-4.5.2/opencv_contrib-4.5.2`).

```bash
sudo apt-get install -y libfreetype6-dev
sudo apt-get install -y libharfbuzz-dev
cd opencv-4.5.2 && mkdir build && cd build
```

```bash
cmake -DCMAKE_INSTALL_PREFIX=/usr \
  -DCMAKE_BUILD_TYPE=Release \
  -DENABLE_CXX11=ON \
  -DBUILD_PERF_TESTS=OFF \
  -DWITH_XINE=ON \
  -DBUILD_TESTS=OFF \
  -DENABLE_PRECOMPILED_HEADERS=OFF \
  -DCMAKE_SKIP_RPATH=ON \
  -DBUILD_WITH_DEBUG_INFO=OFF \
  -DOPENCV_EXTRA_MODULES_PATH=../opencv_contrib-4.5.2/modules \
  -Wno-dev \
  .. && make
```

```bash
sudo make install
```

#### Copy libraries

Copy uscore library before compilation:

```bash
cp ./leltek-echo-native/src/main/cpp/uslib/libuscore.so /etc/webrtc
```

Copy the library after compilation:

```bash
cp ./leltek-echo-native/target/linux-x86_64/libleltek_echo_native.so /etc/webrtc
```

#### Config

[usview config](leltek-echo-native/src/main/resources/usview) must be accessible by SDK.

You can either: 

- place "usview" folder in the users' `HOME` folder.
- place "usview" anywhere and set `USCFG` environment variable. It is a path to "usview" without trailing "/usview".

#### USB permissions

Add the following content to the file `/etc/udev/rules.d/42-custom.rules`:

```
SUBSYSTEM=="usb", ATTRS{idVendor}=="2be5", ATTRS{idProduct}=="0302", MODE="0666"
```

Then launch `udevadm control --reload-rules` or restart a computer. Then unplug and plug the device.
